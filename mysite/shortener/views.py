from django.shortcuts import render
from django.http import HttpResponse, HttpResponsePermanentRedirect
from .models import ShortURL
from django.views.decorators.http import require_POST, require_GET
import random, string
from .forms import UrlShortenFrom


# Create your views here.

@require_GET
def index(request):
    context = {
        'url_shorten_form': UrlShortenFrom()
    }
    return render(request, 'shortener/index.html', context)


@require_GET
def redirect(request, shortcut):
    try:
        short_url = ShortURL.objects.get(shortcut=shortcut)
        short_url.times_visited += 1
        short_url.save()
        return HttpResponsePermanentRedirect(short_url.original_url)
    except ShortURL.DoesNotExist:
        return render(
            request,
            'shortener/404.html',
            {'error_message': 'Couldn\'t find a link for given shortcut: %s' % shortcut}
        )


@require_GET
def info(request, shortcut):
    try:
        short_url = ShortURL.objects.get(shortcut=shortcut)
        context = {
            'shortcut': short_url.shortcut,
            'original_url': short_url.original_url,
            'pub_date': short_url.pub_date,
            'times_visited': short_url.times_visited,
        }
        return render(request, 'shortener/info.html', context)

    except ShortURL.DoesNotExist:
        return render(
            request,
            'shortener/404.html',
            {'error_message': 'Couldn\'t find a link for given shortcut: %s' % shortcut}
        )


@require_POST
def shorten(request):
    form = UrlShortenFrom(request.POST)
    if form.is_valid():
        url = form.cleaned_data['url']
        custom = form.cleaned_data['custom']
        if custom:
            shortcut = custom
        else:
            shortcut = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(6))

        shortened_url = ShortURL.objects.create(original_url=url, shortcut=shortcut)

        context = {
            'shortened_url': shortened_url,
            'domain_name': request.META['HTTP_HOST']
        }

        return render(request, 'shortener/success.html', context)
    else:
        return render(request, 'shortener/index.html', {'url_shorten_form': form})
