from django import forms
from .models import ShortURL
import re


class UrlShortenFrom(forms.Form):
    url = forms.URLField(
        label='Paste an url',
        widget=forms.URLInput(attrs={
            'placeholder': 'Link to be shortened',
            'autocomplete': 'off',
        }))

    check = forms.BooleanField(label="Autocorrect URLs", required=False, initial=True)

    custom = forms.CharField(
        label='Your custom shortcut',
        widget=forms.TextInput(attrs={
            'placeholder': '(Optional) your custom shortcut',
            'autocomplete': 'off',
            'maxlength': '16',
        }),
        required=False)

    def clean_custom(self):
        custom = self.cleaned_data['custom']

        if not custom:
            return

        if len(custom) > 16:
            raise forms.ValidationError('Value too long! Shortcut should be max. 16 characters long')

        if not re.match("^[a-zA-Z0-9]+$", custom):
            raise forms.ValidationError('Shortcut can only consist of letters and digits')

        if custom == 'admin':
            raise forms.ValidationError('Sorry, this shortcut is not available')

        try:
            if ShortURL.objects.filter(shortcut=custom).exists():
                raise forms.ValidationError('\'%s\' shortcut already exists.' % custom)
            return custom
        except OverflowError:
            forms.ValidationError('Value too long! Do you really want to shorten your URL ? :)')
