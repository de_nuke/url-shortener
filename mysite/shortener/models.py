from django.db import models
from django.core.validators import URLValidator
import datetime


# Create your models here.

class ShortURL(models.Model):
    shortcut = models.CharField(max_length=16)
    original_url = models.CharField(max_length=256, validators=[URLValidator()])
    pub_date = models.DateTimeField('date published', auto_now=True)#default=datetime.datetime.now())
    times_visited = models.IntegerField(default=0)

    def __str__(self):
        return str(self.shortcut) + ": " + str(self.original_url)