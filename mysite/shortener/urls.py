from django.conf.urls import url

from . import views

app_name = "shortener"

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<shortcut>[0-9a-zA-Z]+)/$', views.redirect, name='redirect'),
    url(r'^info/(?P<shortcut>[0-9a-zA-Z]+)/$', views.info, name='info'),
    url(r'^shorten$', views.shorten, name="shorten"),

]
