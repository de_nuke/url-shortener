$('.shortlink-copy').click(function(){
    $('#id_shortlink').select();
    document.execCommand('copy');
    $('.shortlink-copy').css('margin-bottom', '10px');
    $('.shortlink-copy').after('<span class="copied-info">Copied!</span>');
    $( '.copied-info' ).fadeOut( "slow", function(){
        $('.copied-info').remove();
        $('.shortlink-copy').css('margin-bottom', '30px');
    });

});

$(".linkinfo-copy").click(function(){
    $('#id_linkinfo').select();
    document.execCommand('copy');
    $('.linkinfo-copy').css('margin-bottom', '10px');
    $('.linkinfo-copy').after('<span class="copied-info">Copied!</span>');
    $( '.copied-info' ).fadeOut( "slow", function(){
        $('.copied-info').remove();
        $('.linkinfo-copy').css('margin-bottom', '30px');
    });
});


$('#id_url').change(correctUrl);
$('#id_check').ready($('#id_check').prop('checked', true));
$('#id_check').click(correctUrl);



function correctUrl() {
    if(document.getElementById('id_check').checked) {

        $('#id_url').val(trim_withmask($('#id_url').val(), './!@#$%^&*()_+-={}[]\\|:;"\',<.>/? '))
        if ($('#id_url').val().search('http://') != 0 && $('#id_url').val().search('https://') != 0  && $('#id_url').val().length > 0 ) {
            $('#id_url').val('http://' + $('#id_url').val());
        } else {
            console.log('not found');
        }
    }
}

function trim_withmask(s, mask) {
    while (~mask.indexOf(s[0])) {
        s = s.slice(1);
    }
    while (~mask.indexOf(s[s.length - 1])) {
        s = s.slice(0, -1);
    }
    return s;
}
